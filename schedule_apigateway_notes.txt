{
   "TableName":"schedule",
   "Key":{
      "schedule_id":{
         "S":"$input.path('$.schedule_id')"
      }
   },
   "UpdateExpression":"set day_name = :day_name ,member = :member ,start_time = :start_time ,end_time = :end_time ,username = :username ,venue_id = :venue_id, venue_name = :venue_name",
   "ExpressionAttributeNames": {
        "#status": "status"
    },
   "ExpressionAttributeValues":{
      ":day_name":{
         "S":"$input.path('$.day_name')"
      },
      ":member":{
         "S":"$input.path('$.member')"
      },
      ":start_time":{
         "S":"$input.path('$.start_time')"
      },
      ":end_time":{
         "S":"$input.path('$.end_time')"
      },
      ":username":{
         "S":"$input.path('$.username')"
      },
      ":venue_id":{
         "S":"$input.path('$.venue_id')"
      },
      ":venue_name":{
         "S":"$input.path('$.venue_name')"
      }
   },
   "ReturnValues":"UPDATED_NEW"
}



{
   "TableName":"schedule",
   "Item":{
      ":schedule_id":{
         "S":"$context.requestId"
      },
      ":day_name":{
         "S":"$input.path('$.day_name')"
      },
      ":member":{
         "S":"$input.path('$.member')"
      },
      ":start_time":{
         "S":"$input.path('$.start_time')"
      },
      ":end_time":{
         "S":"$input.path('$.end_time')"
      },
      ":username":{
         "S":"$input.path('$.username')"
      },
      ":venue_id":{
         "S":"$input.path('$.venue_id')"
      },
      ":venue_name":{
         "S":"$input.path('$.venue_name')"
      }
   }
}