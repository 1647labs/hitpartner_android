package net.hitpartner.app.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.otto.Bus
import com.squareup.otto.Subscribe
import dagger.android.support.DaggerFragment
import net.hitpartner.app.HitPartnerApplication
import javax.inject.Inject

/**
 * Created by markusmcgee on 8/14/17.
 */
abstract class BaseFragment : DaggerFragment() {

    @Inject
    lateinit var app: HitPartnerApplication

    @Inject
    lateinit var bus: Bus

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutResId(), container, false)
        bus.register(this)
        return view
    }

    override fun onDestroyView() {
        bus.unregister(this)
        super.onDestroyView()
    }

    abstract fun getLayoutResId(): Int

    @Subscribe
    abstract fun eventHandler(e: BaseEvent)

}