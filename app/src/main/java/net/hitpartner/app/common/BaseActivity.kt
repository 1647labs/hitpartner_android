package net.hitpartner.app.common

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.squareup.otto.Bus
import dagger.android.support.DaggerAppCompatActivity
import net.hitpartner.app.HitPartnerApplication
import net.hitpartner.app.R
import javax.inject.Inject

/**
 * Created by markusmcgee on 9/12/17.
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var app: HitPartnerApplication

    @Inject
    lateinit var bus: Bus

    open fun getLayoutResId(): Int= R.layout.hitpartner_blank_view

    abstract fun eventHandler(e: BaseEvent)

    private var progressBar: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())

        val layout = findViewById<CoordinatorLayout>(R.id.main_view)
        progressBar = ProgressBar(this)

        if (layout != null) {
            progressBar?.isIndeterminate = true
            progressBar?.visibility = View.GONE
            val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
            layout.addView(progressBar, params)
        }
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    override fun onPause() {
        super.onPause()
        bus.unregister(this)
        closeProgressBar()
    }

    override fun onDestroy() {
        super.onDestroy()
        closeProgressBar()
    }

    fun showProgressBar() {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressBar?.visibility = View.VISIBLE
    }

    fun closeProgressBar() {
        if(progressBar?.visibility == View.VISIBLE){
            progressBar?.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

}