package net.hitpartner.app.common



/**
 * Created by markusmcgee on 8/15/17.
 */
abstract class BaseEvent(val tag: String, val message: String?, val data: Any?)