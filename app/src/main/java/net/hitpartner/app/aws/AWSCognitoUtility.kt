package net.hitpartner.app.aws

import com.amazonaws.auth.AWSCognitoIdentityProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.*
import com.squareup.otto.Bus
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by markusmcgee on 9/4/17.
 */
@Singleton
class AWSCognitoUtility @Inject constructor(private var userPool: CognitoUserPool, private var bus: Bus?) {

    companion object {

        var EMAIL_ATTR: String = "email"
        var PHONE_NUMBER_ATTR: String = "phone_number"
        var NAME_ATTR: String = "name"

        private var currUserAttributes: MutableSet<String>? = null
        lateinit var userSession: CognitoUserSession

        fun clearCurrUserAttributes() {
            currUserAttributes?.clear()
        }

        fun addCurrUserattribute(attribute: String) {
            currUserAttributes?.add(attribute)
        }
    }

    private var password: String? = null

    fun signUpUser(email: String, password: String, phoneNumber: String, name: String) {
        // Read user data and register
        val userAttributes = CognitoUserAttributes()

        userAttributes.addAttribute(EMAIL_ATTR, email)
        userAttributes.addAttribute(PHONE_NUMBER_ATTR, phoneNumber)
        userAttributes.addAttribute(NAME_ATTR, name)

        userPool.signUpInBackground(email, password, userAttributes, null, signUpHandler)
    }

    private var getDetailsHandler: GetDetailsHandler = object : GetDetailsHandler {
        var event: CognitoEvent? = null
        override fun onSuccess(list: CognitoUserDetails) {
            // Successfully retrieved user details
            event = CognitoEvent(CognitoEvent.GET_USER_DETAILS_SUCCESS, "Sign up successful!", list)
            bus?.post(event)
        }

        override fun onFailure(exception: Exception) {
            // Failed to retrieve the user details, probe exception for the cause
            event = CognitoEvent(CognitoEvent.GET_USER_DETAILS_FAILURE, "No User Details", null)
            bus?.post(event)
        }
    }

    private var signUpHandler: SignUpHandler = object : SignUpHandler {

        var event: CognitoEvent? = null

        override fun onSuccess(user: CognitoUser,
                               signUpConfirmationState: Boolean,
                               cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails) {

            if (signUpConfirmationState) {
                // User is already confirmed
                //showDialogMessage("Sign up successful!", username + " has been Confirmed", true)
                event = CognitoEvent(CognitoEvent.ON_SUCCESS, "Sign up successful!", cognitoUserCodeDeliveryDetails)
                bus?.post(event)
            }
            else {
                // User is not confirmed
                event = CognitoEvent(CognitoEvent.ON_CONFIRM_SIGNUP_CALLED, "User is not confirmed", cognitoUserCodeDeliveryDetails)
                bus?.post(event)

            }
        }

        override fun onFailure(exception: Exception) {
            event = CognitoEvent(CognitoEvent.ON_ERROR, exception.message)
            bus?.post(event)
        }
    }

    private var loginConfirmationHandler: GenericHandler = object : GenericHandler {
        var event: CognitoEvent? = null
        override fun onSuccess() {
            event = CognitoEvent(CognitoEvent.ON_CONFIRM_SUCCESS, "Success!", "NAME_GOES_HERE has been confirmed!")
            bus?.post(event)
        }

        override fun onFailure(exception: Exception) {
            event = CognitoEvent(CognitoEvent.ON_CONFIRM_FAILED, "Error!", exception)
            bus?.post(event)
        }
    }

    // Callback handler for the sign-in process
    var authenticationHandler: AuthenticationHandler = object : AuthenticationHandler {

        override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
            AWSCognitoUtility.userSession = userSession!!
            bus?.post(CognitoEvent(CognitoEvent.ON_SIGN_IN_SUCCESS, CognitoEvent.ON_SIGN_IN_SUCCESS, userSession))
        }

        override fun authenticationChallenge(continuation: ChallengeContinuation?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation, userId: String) {
            // The API needs user sign-in credentials to continue
            val authenticationDetails = AuthenticationDetails(userId, password, null)

            // Pass the user sign-in credentials to the continuation
            authenticationContinuation.setAuthenticationDetails(authenticationDetails)

            // Allow the sign-in to continue
            authenticationContinuation.continueTask()
        }

        override fun getMFACode(multiFactorAuthenticationContinuation: MultiFactorAuthenticationContinuation) {
//            // Multi-factor authentication is required; get the verification code from user
//            multiFactorAuthenticationContinuation.setMfaCode(mfaVerificationCode)
//            // Allow the sign-in process to continue
//            multiFactorAuthenticationContinuation.continueTask()
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onFailure(exception: Exception) {
            bus?.post(CognitoEvent(CognitoEvent.ON_SIGNI_IN_FAILURE, exception.message, exception))
        }
    }

    fun resendCode(email: String) {
        val user = userPool.getUser(email)
        user.resendConfirmationCodeInBackground(verificationHandler)
    }

    private var verificationHandler: VerificationHandler = object : VerificationHandler {
        var event: CognitoEvent? = null
        override fun onSuccess(verificationCodeDeliveryMedium: CognitoUserCodeDeliveryDetails?) {
            event = CognitoEvent(CognitoEvent.VERIFICATION_CODE_SENT, CognitoEvent.VERIFICATION_CODE_SENT, null)
            bus?.post(event)
        }

        override fun onFailure(exception: Exception) {
            event = CognitoEvent(CognitoEvent.ON_ERROR, "Error sending verification code", exception)
            bus?.post(event)
        }
    }

    fun confirmSignUpInBackground(email: String, code: String) {
        val user = userPool.getUser(email)
        user.confirmSignUpInBackground(code, false, loginConfirmationHandler)
    }

    fun signInUserInBackground(username: String, password: String) {
        if (username.isEmpty()) {
            bus?.post(CognitoEvent(CognitoEvent.USERNAME_BLANK, "Username cannot be blank"))
            return
        }

        if (password.isEmpty()) {
            bus?.post(CognitoEvent(CognitoEvent.PASSWORD_BLANK, "Password cannot be blank"))
            return
        }

        userPool.currentUser.signOut()
        val cognitoUser: CognitoUser = userPool.getUser(username)
        this.password = password
        cognitoUser.getSessionInBackground(authenticationHandler)

    }

    fun checkIsUserLoggedIn() {
        val cognitoUser: CognitoUser? = userPool.currentUser
        if (cognitoUser?.userId != null) {
            cognitoUser.getSessionInBackground(authenticationHandler)
        }
        else {
            bus?.post(CognitoEvent(CognitoEvent.ON_SIGNI_IN_FAILURE, "Please Sign In", false))
        }
    }

    fun signOut() {
        userPool.currentUser.signOut()
    }

    fun getHitPartners(){


    }
}