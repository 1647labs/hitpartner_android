package net.hitpartner.app.aws

/**
 * Created by markusmcgee on 10/8/17.
 */
object Constants {

    /*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
    val COGNITO_POOL_ID = "us-east-1_yjNQNdvgk"

    val IDENTITY_POOL_ID = "us-east-1:b52ab007-73dc-48d7-a0ac-2461fa8f1124"

    /*
     * Region of your Cognito identity pool ID.
     */
    val COGNITO_POOL_REGION = "us-east-1"

}