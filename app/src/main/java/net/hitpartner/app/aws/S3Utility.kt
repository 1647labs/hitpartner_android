package net.hitpartner.app.aws

import android.content.Context
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.squareup.otto.Bus
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by markusmcgee on 10/8/17.
 */
@Singleton
class S3Utility @Inject constructor(private var userPool: CognitoUserPool?, private var bus: Bus?) {
    // We only need one instance of the clients and credentials provider
    private var sS3Client: AmazonS3Client? = null
    private var sCredProvider: CognitoCachingCredentialsProvider? = null
    private var sTransferUtility: TransferUtility? = null

    /**
     * Gets an instance of the TransferUtility which is constructed using the
     * given Context
     *
     * @param context
     * @return a TransferUtility instance
     */
//    fun getTransferUtility(context: Context): TransferUtility? {
//        if (sTransferUtility == null) {
//            sTransferUtility = TransferUtility(getS3Client(context.getApplicationContext()),
//                    context.getApplicationContext())
//        }
//
//        return sTransferUtility
//    }

    /**
     * Gets an instance of a S3 client which is constructed using the given
     * Context.
     *
     * @param context An Context instance.
     * @return A default S3 client.
     */
//    fun getS3Client(context: Context): AmazonS3Client? {
//    /*    if (sS3Client == null) {
//            sS3Client = AmazonS3Client(getCredProvider(context.applicationContext))
//            sS3Client?.setRegion(Region.getRegion(Regions.fromName(Constants.BUCKET_REGION)))
//        }
//        return sS3Client
//    }*/

    /**
     * Gets an instance of CognitoCachingCredentialsProvider which is
     * constructed using the given Context.
     *
     * @param context An Context instance.
     * @return A default credential provider.
     */
    private fun getCredProvider(context: Context): CognitoCachingCredentialsProvider? {
        if (sCredProvider == null) {
            sCredProvider = CognitoCachingCredentialsProvider(
                    context.applicationContext,
                    Constants.IDENTITY_POOL_ID,
                    Regions.fromName(Constants.COGNITO_POOL_REGION))
        }
        return sCredProvider
    }

//    fun getS3ClientWithUserPoolKeys(): AmazonS3Client? {
//        if (sS3Client == null) {
//            sS3Client = AmazonS3Client(BasicAWSCredentials(Constants.ACCESS_KEY, Constants.SECRET_KEY))
//            sS3Client?.setRegion(Region.getRegion(Regions.fromName(Constants.BUCKET_REGION)))
//        }
//        return sS3Client
//
//    }
}
