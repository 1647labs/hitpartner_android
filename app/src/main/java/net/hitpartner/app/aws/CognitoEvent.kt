package net.hitpartner.app.aws

import net.hitpartner.app.common.BaseEvent


/**
 * Created by markusmcgee on 9/6/17.
 */
class CognitoEvent(tag: String, message: String? = null, data: Any? = null) : BaseEvent(tag, message, data) {
    companion object {
        const val ON_ERROR = "on_error"
        const val ON_SUCCESS = "on_success"
        const val ON_CONFIRM_SIGNUP = "on_confirm_signup"
        const val ON_CONFIRM_SIGNUP_CALLED = "on_confirm_signup_called"
        const val ON_CONFIRM_SUCCESS: String = "on_confirm_success"
        const val ON_CONFIRM_FAILED: String = "on_confirm_failed"
        const val ON_SIGN_IN_SUCCESS: String = "on_sign_in_success"
        const val ON_SIGNI_IN_FAILURE: String = "on_sign_in_failure"
        const val GET_USER_DETAILS_SUCCESS: String = "get_user_details_success"
        const val GET_USER_DETAILS_FAILURE: String = "get_user_details_failure"
        const val LOGOUT: String = "logout"
        const val USERNAME_BLANK: String = "username_blank"
        const val PASSWORD_BLANK: String = "password_blank"
        const val VERIFICATION_CODE_SENT: String = "Verification code sent"
    }
}