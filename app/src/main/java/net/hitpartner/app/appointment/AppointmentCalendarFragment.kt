package net.hitpartner.app.appointment

import android.os.Bundle
import com.squareup.otto.Subscribe
import com.squareup.timessquare.CalendarPickerView
import com.squareup.timessquare.CalendarPickerView.SelectionMode
import kotlinx.android.synthetic.main.appointment_calendar_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.appointment.AppointmentPresenter
import net.hitpartner.app.model.Appointment
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AppointmentCalendarFragment : BaseFragment(), CalendarPickerView.OnDateSelectedListener {

    @Inject
    lateinit var presenter: AppointmentPresenter

    private lateinit var list: ArrayList<Appointment>
    private var formatter = SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm a", Locale.getDefault())

    override fun getLayoutResId(): Int = R.layout.appointment_calendar_view

    companion object {
        private lateinit var fragment: AppointmentCalendarFragment
        const val TAG: String = "AppointmentCalendarFragment"
        fun newInstance(): AppointmentCalendarFragment {
            fragment = AppointmentCalendarFragment()
            return fragment
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getAppointment(false)
    }

    private fun updateCalendar() {
        val dateList = mutableListOf<Date>()
        for (a in list) {
            val date = formatter.parse(a.datecreated)
            dateList.add(date)
        }

        val nextYear = Calendar.getInstance()
        nextYear.add(Calendar.YEAR, 2)

        val lastYear = Calendar.getInstance()
        lastYear.add(Calendar.YEAR, -2)

        val calendar = view?.findViewById<CalendarPickerView>(R.id.calendar_view)
        calendar?.init(lastYear.time, nextYear.time)
                ?.inMode(SelectionMode.MULTIPLE)
                ?.displayOnly()
                ?.withSelectedDates(dateList)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        calendar_view.setOnDateSelectedListener(this)
    }

    override fun onDateSelected(date: Date?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDateUnselected(date: Date?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is AppointmentEvent && e.tag == AppointmentEvent.APPOINTMENT_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                list = e.data as ArrayList<Appointment>
                updateCalendar()
            }
        }
    }
}