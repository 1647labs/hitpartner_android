package net.hitpartner.app.appointment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.TimePicker
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.appointment_edit_fragment_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.appointment.AppointmentPresenter
import net.hitpartner.app.di.profile.ProfilePresenter
import net.hitpartner.app.di.schedule.SchedulePresenter
import net.hitpartner.app.di.venue.VenuePresenter
import net.hitpartner.app.model.Appointment
import net.hitpartner.app.model.Profile
import net.hitpartner.app.model.Venue
import net.hitpartner.app.profile.HitPartnerAdapter
import net.hitpartner.app.profile.ProfileEvent
import net.hitpartner.app.schedule.ScheduleVenueAdapter
import net.hitpartner.app.venue.VenueEvent
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class AppointmentEditFragment : BaseFragment(), TimePickerDialog.OnTimeSetListener {

    private lateinit var appointmentType: String

    private lateinit var currentTimeView: TextView

    @Inject
    lateinit var schedulePresenter: SchedulePresenter

    @Inject
    lateinit var venuePresenter: VenuePresenter

    @Inject
    lateinit var appointmentPresenter: AppointmentPresenter

    @Inject
    lateinit var profilePresenter: ProfilePresenter


    override fun getLayoutResId() = R.layout.appointment_edit_fragment_view

    companion object {
        const val TAG: String = "EditProfileFragment"
        const val APPOINTMENT_HITPARTNER = "hitpartner"
        const val APPOINTMENT_VENUE = "venue"

        fun newInstance(): AppointmentEditFragment {
            return AppointmentEditFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        appointmentType = arguments?.getString("appointmentType", "venue").toString()
        when (appointmentType) {
            APPOINTMENT_HITPARTNER -> {
//                app.userPool.

            }
            APPOINTMENT_VENUE -> {

            }
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        start_time_text.setOnClickListener { timeDialogHandler(start_time_text) }
        end_time_text.setOnClickListener { timeDialogHandler(end_time_text) }
        date_text.setOnClickListener { calendarDialog(date_text) }

        ok_button.setOnClickListener { view -> okCancelButtonHandler(view) }
        cancel_button.setOnClickListener { view -> okCancelButtonHandler(view) }
    }

    private fun okCancelButtonHandler(view: View) {
        when (view) {
            ok_button -> {

                val now = Date().time.toString()
                val hitpartner = hitpartner_spinner.selectedItem as Profile
                val venue = venue_spinner.selectedItem as Venue

                val appointment = Appointment()
                appointment.hitpartner_id = hitpartner.username
                appointment.hitpartner_name = String.format("%s %s", hitpartner.firstname, hitpartner.lastname)

                appointment.venue_id = venue.venue_id
                appointment.venue_name = venue.venue_name
                appointment.venue_location = venue.address
                appointment.venue_address = venue.address

                appointment.start_time = start_time_text.text.toString()
                appointment.end_time = end_time_text.text.toString()
                appointment.duration = "1"
                appointment.status = "pending"
                appointment.rate = "75"
                appointment.rate_type = "per hour"

                appointment.datecreated = now
                appointment.datecreated_timestamp = now
                appointment.datemodified = now

                appointmentPresenter.addAppointment(appointment)

            }
            cancel_button -> {
                Log.d("debug", "debug")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        venuePresenter.getVenues(true)
        profilePresenter.getHitPartnerProfiles()
    }

    private fun calendarDialog(incomingView: TextView) {
        val c = Calendar.getInstance(Locale.getDefault())
        val formatter = SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault())

        // Launch Time Picker Dialog
        val datePickerDialog = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    run {
                        val newDate = Calendar.getInstance()
                        newDate.set(year, monthOfYear, dayOfMonth)
                        incomingView.text = formatter.format(newDate.time)
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.show()
    }


    private fun timeDialogHandler(incomingView: TextView) {
        // Get Current Time
        val c = Calendar.getInstance(Locale.getDefault())
        val mHour = 0 //c.get(Calendar.HOUR)
        val mMinute = 0 //c.get(Calendar.MINUTE) % 15 + 15

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(context,
                this, mHour, mMinute, false)
        currentTimeView = incomingView

        timePickerDialog.show()
    }

    override fun onTimeSet(timePicker: TimePicker?, hourOfDay: Int, minute: Int) {
        val c = Calendar.getInstance(Locale.getDefault())
        val ampm = when (c.get(Calendar.AM_PM)) {
            Calendar.AM -> "AM"
            else -> "PM"
        }
        currentTimeView.text = String.format("%02d:%02d: %s", hourOfDay, minute, ampm)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Venue>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                val adapter = ScheduleVenueAdapter(context, android.R.layout.simple_list_item_2, list.toMutableList())
                venue_spinner.adapter = adapter
            }
        } else if (e is ProfileEvent && e.tag == ProfileEvent.PROFILES_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Profile>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                val adapter = HitPartnerAdapter(context, android.R.layout.simple_list_item_2, list.toMutableList())
                hitpartner_spinner.adapter = adapter
            }
        } else if (e is AppointmentEvent && e.tag == AppointmentEvent.APPOINTMENT_CREATED) {
            activity?.runOnUiThread {
                Snackbar.make(view!!, "Add Appointment Successful", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
            }
        }
    }
}