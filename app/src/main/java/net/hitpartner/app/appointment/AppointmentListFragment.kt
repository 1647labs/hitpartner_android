package net.hitpartner.app.appointment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_appointments_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.appointment.AppointmentPresenter
import net.hitpartner.app.model.Appointment
import javax.inject.Inject

class AppointmentListFragment : BaseFragment(), AppointmentAdapter.OnClickHandler {

    private var appointmentType: String = ""

    @Inject
    lateinit var presenter: AppointmentPresenter

    override fun getLayoutResId(): Int = R.layout.fragment_appointments_view

    companion object {
        private lateinit var fragment: AppointmentListFragment
        const val TAG: String = "AppointmentListFragment"
        fun newInstance(): AppointmentListFragment {
            fragment = AppointmentListFragment()
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fab.setOnClickListener { view -> onClickListener(view) }
        appointmentType = resources.getStringArray(R.array.appointment_types)[0] //default value
    }

    private fun onClickListener(view: View) {
        when (view) {
            fab -> {
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle(R.string.add_appointment)
                builder.setSingleChoiceItems(R.array.appointment_types, 0) { dialogInterface, i ->
                    appointmentType = resources.getStringArray(R.array.appointment_types)[i]
                }
                builder.setPositiveButton(android.R.string.ok) { dialog, id ->
                    addAppointmentHandler()
                }
                builder.setNegativeButton(android.R.string.cancel) { dialog, id ->
                    dialog.dismiss()
                    Snackbar.make(fab, "Cancelled - New Appointment", Snackbar.LENGTH_LONG)
                            .show()
                }
                builder.create().show()
            }
        }
    }

    private fun addAppointmentHandler() {
        bus.post(AppointmentEvent(AppointmentEvent.APPOINTMENT_NEW, AppointmentEvent.APPOINTMENT_NEW, appointmentType))
        Snackbar.make(fab, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    override fun onResume() {
        super.onResume()
        presenter.getAppointment(true)
    }

    override fun onItemClickHandler(appointment: Appointment) {
        bus.post(AppointmentEvent(AppointmentEvent.APPOINTMENT_DETAIL, AppointmentEvent.APPOINTMENT_DETAIL, appointment))
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is AppointmentEvent && e.tag == AppointmentEvent.APPOINTMENT_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Appointment>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                appointment_recycler_view.layoutManager = llm
                appointment_recycler_view.adapter = AppointmentAdapter(list, this)
            }
        }
    }
}