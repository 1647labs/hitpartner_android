package net.hitpartner.app.appointment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.hitpartner.app.R
import net.hitpartner.app.model.Appointment


class AppointmentAdapter constructor(private val items: ArrayList<Appointment>,
                                     private val listener: OnClickHandler) : RecyclerView.Adapter<AppointmentAdapter.ViewHolder>() {

    interface OnClickHandler {
        fun onItemClickHandler(appointment: Appointment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.appointment_list_item, parent, false))
        viewHolder.itemView.setOnClickListener { listener.onItemClickHandler(items.get(viewHolder.layoutPosition)) }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.hitpartner_name.text = items.get(position).hitpartner_name
        holder.venue_location.text = items.get(position).venue_location
        holder.venue_name.text = items.get(position).venue_name
        holder.start_time.text = items.get(position).start_time
        holder.end_time.text = items.get(position).end_time
        holder.duration.text = items.get(position).duration
        holder.rate.text = items.get(position).rate
        holder.rate_type.text = items.get(position).rate_type
        holder.status.text = items.get(position).status
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val hitpartner_name = view.findViewById<TextView>(R.id.hitpartner_name)
        val venue_name = view.findViewById<TextView>(R.id.venue_name)
        val venue_location = view.findViewById<TextView>(R.id.venue_location)
        val start_time = view.findViewById<TextView>(R.id.start_time_text)
        val end_time = view.findViewById<TextView>(R.id.end_time_text)
        val duration = view.findViewById<TextView>(R.id.duration_text)
        val rate = view.findViewById<TextView>(R.id.rate)
        val rate_type = view.findViewById<TextView>(R.id.rate_type)
        val status = view.findViewById<TextView>(R.id.status_text)
    }
}