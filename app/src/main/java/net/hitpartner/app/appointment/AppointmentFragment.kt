package net.hitpartner.app.appointment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_appointment_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.appointment.AppointmentPresenter
import net.hitpartner.app.di.schedule.SchedulePresenter
import net.hitpartner.app.di.venue.VenuePresenter
import net.hitpartner.app.model.Appointment
import net.hitpartner.app.model.Venue
import net.hitpartner.app.schedule.ScheduleVenueAdapter
import net.hitpartner.app.venue.VenueEvent
import java.util.*
import javax.inject.Inject

class AppointmentFragment : BaseFragment() {

    lateinit var appointment: Appointment

    @Inject
    lateinit var schedulePresenter: SchedulePresenter

    @Inject
    lateinit var venuePresenter: VenuePresenter

    @Inject
    lateinit var appointmentPresenter: AppointmentPresenter

    override fun getLayoutResId() = R.layout.fragment_appointment_view

    companion object {
        const val TAG: String = "AppointmentFragment"
        private lateinit var fragment: AppointmentFragment
        fun newInstance(): AppointmentFragment {
            fragment = AppointmentFragment()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        appointment = arguments?.getSerializable("appointment") as Appointment
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        status_text.text = appointment.status
        rate_text.text = String.format("$%s %s per hour", appointment.rate, appointment.rate_type)
        date_text.text = appointment.datecreated
        start_time_text.text = appointment.start_time
        end_time_text.text = appointment.end_time
        hitpartner_name_text.text = appointment.hitpartner_name
        venue_name_text.text = appointment.venue_name
        venue_location_text.text = appointment.venue_address
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Venue>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                val adapter = ScheduleVenueAdapter(context, android.R.layout.simple_list_item_2, list.toMutableList())
//                venue_spinner.adapter = adapter
            }
        }
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
        }
    }
}