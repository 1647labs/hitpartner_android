package net.hitpartner.app.appointment

import net.hitpartner.app.common.BaseEvent

class AppointmentEvent(tag: String, message: String? = null, data: Any? = null) : BaseEvent(tag, message, data) {
    companion object {
        const val APPOINTMENT_NEW: String = "Appointment New"
        const val APPOINTMENT_DETAIL: String = "Appointment Detail"
        const val APPOINTMENT_UPDATED: String = "Appointment Updated"
        const val APPOINTMENT_CREATED: String = "Appointment Created"
        const val APPOINTMENT_LOAD_SUCCESS: String = "Appointment Load Success"
    }
}