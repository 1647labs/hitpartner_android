package net.hitpartner.app

import android.content.Intent
import android.os.Bundle
import net.hitpartner.app.common.BaseActivity
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.login.LoginActivity

class HitPartnerActivity : BaseActivity() {

    override fun getLayoutResId(): Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
    }

    override fun onResume() {
        super.onResume()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun eventHandler(e: BaseEvent) {
        // To change body of created functions use File | Settings | File Templates.
        //  TODO("not implemented")
    }
}