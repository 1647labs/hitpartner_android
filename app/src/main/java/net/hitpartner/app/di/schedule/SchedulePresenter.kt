package net.hitpartner.app.di.schedule

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.hitpartner.app.HitPartnerApplication
import net.hitpartner.app.model.Schedule
import net.hitpartner.app.model.Venue
import net.hitpartner.app.schedule.ScheduleEvent
import net.hitpartner.app.service.ScheduleService
import javax.inject.Inject

class SchedulePresenter @Inject constructor() : ScheduleContract.SchedulePresenter {

    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var app: HitPartnerApplication

    override fun addSchedule(schedule: Schedule) {
        val service = ScheduleService.create()
        service.addSchedule(schedule).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_ERROR, ScheduleEvent.SCHEDULE_LOAD_ERROR, null)
                    bus.post(event)
                }
                .doOnNext { scheduleResponse ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("schedule", Gson().toJson(scheduleResponse))
                        editor.commit()

                        val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_SUCCESS, ScheduleEvent.SCHEDULE_LOAD_SUCCESS, scheduleResponse)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun updateSchedule(schedule: Schedule) {
        val service = ScheduleService.create()
        service.updateSchedule(schedule).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    val event = ScheduleEvent(ScheduleEvent.SCHEDULE_UPDATE_ERROR, ScheduleEvent.SCHEDULE_UPDATE_ERROR, null)
                    bus.post(event)
                }
                .doOnNext { scheduleResponse ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("schedule", Gson().toJson(scheduleResponse))
                        editor.commit()

                        val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_SUCCESS, ScheduleEvent.SCHEDULE_LOAD_SUCCESS, scheduleResponse)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }

    }

    override fun getSchedule(forceRefresh: Boolean) {
        when {
            forceRefresh      -> getValuesFromService()
            hasCachedValues() -> getCachedValues()
            else              -> getValuesFromService()
        }
    }

    private fun getCachedValues() {
        val venues: List<Venue>

        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        val scheduleStr = prefs.getString("schedule", "")
        val scheduleListType = object : TypeToken<ArrayList<Schedule>>() {}.type

        venues = Gson().fromJson(scheduleStr, scheduleListType)
        val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_SUCCESS, ScheduleEvent.SCHEDULE_LOAD_SUCCESS, venues)
        bus.post(event)
    }

    private fun getValuesFromService() {
        val service = ScheduleService.create()
        service.getSchedule().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_SUCCESS, ScheduleEvent.SCHEDULE_LOAD_SUCCESS, null)
                    bus.post(event)
                }
                .doOnNext { scheduleResponse ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("schedule", Gson().toJson(scheduleResponse))
                        editor.commit()

                        val event = ScheduleEvent(ScheduleEvent.SCHEDULE_LOAD_ERROR, ScheduleEvent.SCHEDULE_LOAD_ERROR, scheduleResponse)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }


    private fun hasCachedValues(): Boolean {
        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        return !prefs.getString("schedule", "").isNullOrEmpty()
    }

}