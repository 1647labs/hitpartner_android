package net.hitpartner.app.di.venue

import dagger.Binds
import dagger.Module

@Module
abstract class VenueModule {
    @Binds
    abstract fun venuePresenter(presenter: VenuePresenter): VenueContract.VenuePresenter
}