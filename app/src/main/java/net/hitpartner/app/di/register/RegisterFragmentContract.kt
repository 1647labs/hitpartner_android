package net.hitpartner.app.di.register

import net.hitpartner.app.model.Profile

interface RegisterFragmentContract {
    interface RegisterFragmentPresenter {
        fun registerUser(email: String, password: String, phoneNumber: String)
        fun updateUserProfile(userProfile: Profile)
        fun addProfileDetails(userProfile: Profile)
        fun verifyUser(email: String, verifyCode: String)
        fun resendCode(email: String)
    }

}