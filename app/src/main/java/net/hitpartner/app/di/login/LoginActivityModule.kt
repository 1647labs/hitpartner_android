package net.hitpartner.app.di.login

import dagger.Binds
import dagger.Module

@Module
abstract class LoginActivityModule {
    @Binds
    abstract fun loginActivityPresenter(presenter: LoginActivityPresenter): LoginActivityContract.LoginActivityPresenter
}