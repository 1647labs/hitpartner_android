package net.hitpartner.app.di.schedule

import net.hitpartner.app.model.Schedule

interface ScheduleContract {
    interface SchedulePresenter {
        fun getSchedule(forceRefresh: Boolean = true)
        fun addSchedule(schedule: Schedule)
        fun updateSchedule(schedule: Schedule)
    }
}