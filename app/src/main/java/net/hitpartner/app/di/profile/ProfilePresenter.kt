package net.hitpartner.app.di.profile

import android.util.Log
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.hitpartner.app.model.Profile
import net.hitpartner.app.profile.ProfileEvent
import net.hitpartner.app.service.ProfileService
import javax.inject.Inject

class ProfilePresenter @Inject constructor() : ProfileContract.ProfilePresenter {

    @Inject
    lateinit var bus: Bus

    override fun getUserProfile(email: String) {
        val service = ProfileService.create()
        service.getUserProfileByEmail(email).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext {
                    val event = ProfileEvent(ProfileEvent.PROFILE_LOAD_SUCCESS, ProfileEvent.PROFILE_LOAD_SUCCESS, it)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun getHitPartnerProfiles() {
        val service = ProfileService.create()
        service.getHitPartners().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext { response ->
                    val event = ProfileEvent(ProfileEvent.PROFILES_LOAD_SUCCESS, ProfileEvent.PROFILES_LOAD_SUCCESS, response.profileList)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun saveUserProfile(profile: Profile) {
        val service = ProfileService.create()
        service.addProfileDetails(profile).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext {
                    val event = ProfileEvent(ProfileEvent.PROFILE_CREATED, ProfileEvent.PROFILE_CREATED, it)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun updateUserProfile(profile: Profile) {
        val service = ProfileService.create()
        service.updateProfileDetails(profile).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext {
                    val event = ProfileEvent(ProfileEvent.PROFILE_UPDATED, ProfileEvent.PROFILE_UPDATED)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }
}