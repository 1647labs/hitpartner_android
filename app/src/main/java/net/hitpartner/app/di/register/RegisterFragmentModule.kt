package net.hitpartner.app.di.register

import dagger.Binds
import dagger.Module

@Module
abstract class RegisterFragmentModule  {
    @Binds
    abstract fun registerFragmentPresenter(presenter: RegisterFragmentPresenter) : RegisterFragmentContract.RegisterFragmentPresenter
}