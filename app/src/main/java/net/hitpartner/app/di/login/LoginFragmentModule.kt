package net.hitpartner.app.di.login

import dagger.Binds
import dagger.Module

/**
 * Created by Rakesh Das on 1/13/18.
 */
@Module
abstract class LoginFragmentModule {
    @Binds
    abstract fun loginFragmentPresenter(presenter: LoginFragmentPresenter): LoginFragmentContract.LoginFragmentPresenter
}