package net.hitpartner.app.di.appointment

import net.hitpartner.app.model.Appointment

interface AppointmentContract {
    interface AppointmentPresenter {
        fun getAppointment(appointment_id: String, forceRefresh: Boolean = true)
        fun getAppointment(forceRefresh: Boolean = true)
        fun addAppointment(appointment:Appointment)
    }

    interface MockAppointmentPresenter {
        fun getAppointment(appointment_id: String, forceRefresh: Boolean = true)
        fun getAppointment(forceRefresh: Boolean = true)
    }
}