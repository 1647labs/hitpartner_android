package net.hitpartner.app.di.login

import android.content.Context

/**
 * Created by Rakesh Das on 1/20/18.
 */
interface LoginFragmentContract {

    interface LoginFragmentView {
        // button events
        fun onLoginButtonClicked()
    }
    interface LoginFragmentPresenter {
        fun submitLoginDetails(context: Context)
    }
}