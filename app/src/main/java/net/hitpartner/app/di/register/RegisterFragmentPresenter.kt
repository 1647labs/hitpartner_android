package net.hitpartner.app.di.register

import android.util.Log
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.hitpartner.app.HitPartnerApplication
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.model.Profile
import net.hitpartner.app.profile.ProfileEvent
import net.hitpartner.app.service.ProfileService
import javax.inject.Inject

class RegisterFragmentPresenter @Inject constructor() : RegisterFragmentContract.RegisterFragmentPresenter {
    @Inject
    lateinit var app: HitPartnerApplication

    @Inject
    lateinit var bus: Bus

    override fun registerUser(email: String, password: String, phoneNumber: String) {
        AWSCognitoUtility(app.userPool, bus).signUpUser(email, password, phoneNumber, "name")
    }

    override fun addProfileDetails(userProfile: Profile) {
        val userProfileService = ProfileService.create()
        userProfileService.addProfileDetails(userProfile).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext {
                    val event = ProfileEvent(ProfileEvent.PROFILE_CREATED, ProfileEvent.PROFILE_CREATED, it)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun updateUserProfile(userProfile: Profile) {
        val userProfileService = ProfileService.create()
        userProfileService.updateProfileDetails(userProfile).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext {
                    val event = ProfileEvent(ProfileEvent.PROFILE_UPDATED, ProfileEvent.PROFILE_UPDATED, it)
                    bus.post(event)
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }

    override fun verifyUser(email: String, verifyCode: String) {
        AWSCognitoUtility(app.userPool, bus).confirmSignUpInBackground(email, verifyCode)
    }

    override fun resendCode(email: String) {
        AWSCognitoUtility(app.userPool, bus).resendCode(email)
    }

}