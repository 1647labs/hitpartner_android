package net.hitpartner.app.di.appointment

import dagger.Binds
import dagger.Module
import net.hitpartner.app.di.schedule.ScheduleContract
import net.hitpartner.app.di.schedule.SchedulePresenter
import net.hitpartner.app.di.venue.VenueContract
import net.hitpartner.app.di.venue.VenuePresenter

@Module
abstract class AppointmentModule {

    @Binds
    abstract fun appointmentPresenter(appointmentPresenter: AppointmentPresenter): AppointmentContract.AppointmentPresenter

    @Binds
    abstract fun venuePresenter(venuePresenter: VenuePresenter): VenueContract.VenuePresenter

    @Binds
    abstract fun schedulePresenter(schedulePresenter: SchedulePresenter): ScheduleContract.SchedulePresenter

}