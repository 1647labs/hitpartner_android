package net.hitpartner.app.di.login

import dagger.Binds
import dagger.Module

/**
 * Created by Rakesh Das on 1/13/18.
 */
@Module
abstract class ConfirmUserRegistrationFragmentModule {
    @Binds
    abstract fun confirmUserRegistrationActivityPresenter(presenter: ConfirmUserRegistrationFragmentContract.ConfirmUserRegistrationActivityPresenter): ConfirmUserRegistrationFragmentContract.ConfirmUserRegistrationActivityPresenter
}