package net.hitpartner.app.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hitpartner.app.HitPartnerActivity
import net.hitpartner.app.NavigationActivity
import net.hitpartner.app.di.login.LoginActivityModule
import net.hitpartner.app.login.LoginActivity

@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = arrayOf(HitPartnerActivityModule::class))
    abstract fun inject_HitPartnerActivity(): HitPartnerActivity

    @ContributesAndroidInjector(modules = arrayOf(LoginActivityModule::class))
    abstract fun inject_LoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = arrayOf(NavigationActivityModule::class))
    abstract fun inject_NavigationActivity(): NavigationActivity

}