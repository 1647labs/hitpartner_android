package net.hitpartner.app.di

import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilityModule {

    @Provides
    @Singleton
    fun provideBus(): Bus {
        return Bus(ThreadEnforcer.ANY)
    }

}