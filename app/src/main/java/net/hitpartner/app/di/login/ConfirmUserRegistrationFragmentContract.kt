package net.hitpartner.app.di.login

import android.content.Context

/**
 * Created by Rakesh Das on 1/20/18.
 */
interface ConfirmUserRegistrationFragmentContract {

    interface ConfirmUserRegistrationFragmentView {
        // button events
        fun onConfirmUserButtonClick()
    }
    interface ConfirmUserRegistrationActivityPresenter {
        fun submitConfirmUserDetails(context: Context)
    }
}