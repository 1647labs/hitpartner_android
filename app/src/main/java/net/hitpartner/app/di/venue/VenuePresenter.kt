package net.hitpartner.app.di.venue

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.hitpartner.app.HitPartnerApplication
import net.hitpartner.app.model.Venue
import net.hitpartner.app.service.VenueService
import net.hitpartner.app.venue.VenueEvent
import javax.inject.Inject


class VenuePresenter @Inject constructor() : VenueContract.VenuePresenter {
    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var app: HitPartnerApplication

    override fun getVenues(forceRefresh: Boolean) {
        when {
            forceRefresh -> getValuesFromService()
            hasCachedValues() -> getCachedValues()
            else -> getValuesFromService()
        }
    }

    private fun getCachedValues() {
        val venues: List<Venue>

        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        val venueStr = prefs.getString("venues", "")
        val venueListType = object : TypeToken<ArrayList<Venue>>() {}.type

        venues = Gson().fromJson(venueStr, venueListType)
        val event = VenueEvent(VenueEvent.VENUE_LOAD_SUCCESS, VenueEvent.VENUE_LOAD_SUCCESS, venues)
        bus.post(event)
    }

    private fun getValuesFromService() {
        val service = VenueService.create()
        service.getVenues().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    run {
                        val event = VenueEvent(VenueEvent.VENUE_LOAD_ERROR, VenueEvent.VENUE_LOAD_ERROR, null)
                        bus.post(event)
                    }
                }
                .doOnNext { response ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("venues", Gson().toJson(response.venueList))
                        editor.commit()

                        val event = VenueEvent(VenueEvent.VENUE_LOAD_SUCCESS, VenueEvent.VENUE_LOAD_SUCCESS, response.venueList)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }


    private fun hasCachedValues(): Boolean {
        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        return !prefs.getString("venues", "").isNullOrEmpty()
    }

    override fun getVenue(venue_id: String, forceRefresh: Boolean) {
        val service = VenueService.create()

        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        val venueStr = prefs.getString("venues", "")
        val venueListType = object : TypeToken<ArrayList<Venue>>() {}.type

        if (forceRefresh) {
            service.getVenueByName(venue_id).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        run {
                            val event = VenueEvent(VenueEvent.VENUE_LOAD_ERROR, VenueEvent.VENUE_LOAD_ERROR, null)
                            bus.post(event)
                        }
                    }
                    .doOnNext { venues ->
                        run {

                            val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                            editor.putString("venues", Gson().toJson(venues))
                            editor.commit()

                            val event = VenueEvent(VenueEvent.VENUE_LOAD_SUCCESS, VenueEvent.VENUE_LOAD_SUCCESS, venues)
                            bus.post(event)
                        }
                    }
                    .subscribe {
                        Log.d("debug", "debug")
                    }

        } else {
            val venues: List<Venue> = Gson().fromJson(venueStr, venueListType)
            val event = VenueEvent(VenueEvent.VENUE_LOAD_SUCCESS, VenueEvent.VENUE_LOAD_SUCCESS, venues)
            bus.post(event)
        }
    }
}