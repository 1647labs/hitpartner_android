package net.hitpartner.app.di.profile

import dagger.Binds
import dagger.Module

/**
 * Created by Markus McGee on 7/27/18.
 */
@Module
abstract class ProfileModule {
    @Binds
    abstract fun profileFragmentPresenter(presenter: ProfilePresenter): ProfileContract.ProfilePresenter
}