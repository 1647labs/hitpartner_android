package net.hitpartner.app.di.venue

import net.hitpartner.app.model.Schedule

interface VenueContract {
    interface VenuePresenter {
        fun getVenue(venue_id: String, forceRefresh:Boolean = true)
        fun getVenues(forceRefresh:Boolean = true)

    }
}