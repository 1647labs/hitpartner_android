package net.hitpartner.app.di.schedule

import dagger.Binds
import dagger.Module

@Module
abstract class ScheduleModule {
    @Binds
    abstract fun schedulePresenter(presenter: SchedulePresenter): ScheduleContract.SchedulePresenter
}