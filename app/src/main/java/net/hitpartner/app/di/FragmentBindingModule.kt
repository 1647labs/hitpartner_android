package net.hitpartner.app.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.hitpartner.app.appointment.AppointmentCalendarFragment
import net.hitpartner.app.appointment.AppointmentEditFragment
import net.hitpartner.app.appointment.AppointmentFragment
import net.hitpartner.app.appointment.AppointmentListFragment
import net.hitpartner.app.di.appointment.AppointmentModule
import net.hitpartner.app.di.login.ConfirmUserRegistrationFragmentModule
import net.hitpartner.app.di.login.LoginFragmentModule
import net.hitpartner.app.di.profile.ProfileModule
import net.hitpartner.app.di.register.NewUserVerificationFragmentModule
import net.hitpartner.app.di.register.RegisterFragmentModule
import net.hitpartner.app.di.schedule.ScheduleModule
import net.hitpartner.app.di.venue.VenueModule
import net.hitpartner.app.login.ConfirmUserRegistrationFragment
import net.hitpartner.app.login.LoginFragment
import net.hitpartner.app.login.NewUserVerificationFragment
import net.hitpartner.app.login.RegisterFragment
import net.hitpartner.app.profile.EditProfileFragment
import net.hitpartner.app.profile.ProfileFragment
import net.hitpartner.app.schedule.ScheduleEditFragment
import net.hitpartner.app.schedule.ScheduleFragment
import net.hitpartner.app.schedule.ScheduleListFragment
import net.hitpartner.app.venue.VenueEditFragment
import net.hitpartner.app.venue.VenueFragment
import net.hitpartner.app.venue.VenueListFragment

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector(modules = arrayOf(LoginFragmentModule::class))
    abstract fun inject_LoginFragment(): LoginFragment

    @ContributesAndroidInjector(modules = arrayOf(RegisterFragmentModule::class))
    abstract fun inject_RegisterFragment(): RegisterFragment

    @ContributesAndroidInjector(modules = arrayOf(NewUserVerificationFragmentModule::class))
    abstract fun inject_NewUserVerificationFragment(): NewUserVerificationFragment

    @ContributesAndroidInjector(modules = arrayOf(ProfileModule::class))
    abstract fun inject_ProfileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = arrayOf(ProfileModule::class))
    abstract fun inject_EditProfileFragment(): EditProfileFragment

    @ContributesAndroidInjector(modules = arrayOf(ConfirmUserRegistrationFragmentModule::class))
    abstract fun inject_ConfirmUserRegistrationFragment(): ConfirmUserRegistrationFragment

    @ContributesAndroidInjector(modules = arrayOf(AppointmentModule::class))
    abstract fun inject_AppointmentListFragment(): AppointmentListFragment

    @ContributesAndroidInjector(modules = arrayOf(AppointmentModule::class))
    abstract fun inject_AppointmentEditFragment(): AppointmentEditFragment

    @ContributesAndroidInjector(modules = arrayOf(AppointmentModule::class))
    abstract fun inject_AppointmentCalendarFragment(): AppointmentCalendarFragment

    @ContributesAndroidInjector(modules = arrayOf(AppointmentModule::class))
    abstract fun inject_AppointmentFragment(): AppointmentFragment

    @ContributesAndroidInjector(modules = arrayOf(VenueModule::class))
    abstract fun inject_VenueFragment(): VenueFragment

    @ContributesAndroidInjector(modules = arrayOf(VenueModule::class))
    abstract fun inject_VenueEditFragment(): VenueEditFragment

    @ContributesAndroidInjector(modules = arrayOf(VenueModule::class))
    abstract fun inject_VenueListFragment(): VenueListFragment

    @ContributesAndroidInjector(modules = arrayOf(ScheduleModule::class))
    abstract fun inject_ScheduleFragment(): ScheduleFragment

    @ContributesAndroidInjector(modules = arrayOf(ScheduleModule::class))
    abstract fun inject_ScheduleEditFragment(): ScheduleEditFragment

    @ContributesAndroidInjector(modules = arrayOf(ScheduleModule::class))
    abstract fun inject_ScheduleListFragment(): ScheduleListFragment
}