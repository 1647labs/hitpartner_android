package net.hitpartner.app.di.profile

import net.hitpartner.app.model.Profile

/**
 * Created by Rakesh Das on 3/31/18.
 */

interface ProfileContract {
    interface ProfilePresenter {
        fun saveUserProfile(profile: Profile)
        fun updateUserProfile(profile: Profile)

        fun getUserProfile(email: String)
        fun getHitPartnerProfiles()
    }

}