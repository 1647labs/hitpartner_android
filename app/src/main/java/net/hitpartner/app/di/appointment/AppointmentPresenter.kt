package net.hitpartner.app.di.appointment

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.otto.Bus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.hitpartner.app.HitPartnerApplication
import net.hitpartner.app.appointment.AppointmentEvent
import net.hitpartner.app.model.Appointment
import net.hitpartner.app.service.AppointmentService
import javax.inject.Inject


class AppointmentPresenter @Inject constructor() : AppointmentContract.AppointmentPresenter {

    @Inject
    lateinit var bus: Bus

    @Inject
    lateinit var app: HitPartnerApplication


    //TODO: A try catch should be used on the event of failure here.  The fall back is hitting the server.
    override fun getAppointment(forceRefresh: Boolean) {
        when {
            forceRefresh -> getValuesFromService()
            hasCachedValues() -> getCachedValues()
            else -> getValuesFromService()
        }
    }

    private fun getCachedValues() {
        val appointments: List<Appointment>

        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        val appointmentStr = prefs.getString("appointments", "")
        val appointmentListType = object : TypeToken<ArrayList<Appointment>>() {}.type

        appointments = Gson().fromJson(appointmentStr, appointmentListType)
        val event = AppointmentEvent(AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, appointments)
        bus.post(event)
    }

    private fun getValuesFromService() {
        val service = AppointmentService.create()
        service.getAppointment().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext { appointments ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("appointments", Gson().toJson(appointments))
                        editor.commit()

                        val event = AppointmentEvent(AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, appointments.appointmentList)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }


    private fun hasCachedValues(): Boolean {
        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        return !prefs.getString("appointments", "").isNullOrEmpty()
    }


    override fun addAppointment(appointment: Appointment) {
        val service = AppointmentService.create()
        service.addAppointment(appointment).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    Log.d("debug", "debug")
                }
                .doOnNext { appointmentResponse ->
                    run {

                        val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                        editor.putString("appointment", Gson().toJson(appointmentResponse))
                        editor.commit()

                        val event = AppointmentEvent(AppointmentEvent.APPOINTMENT_CREATED, AppointmentEvent.APPOINTMENT_CREATED, appointmentResponse)
                        bus.post(event)
                    }
                }
                .subscribe {
                    Log.d("debug", "debug")
                }
    }


    override fun getAppointment(appointment_id: String, forceRefresh: Boolean) {
        val service = AppointmentService.create()

        val appointments: List<Appointment>

        val prefs = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE)
        val appointmentStr = prefs.getString("appointments", "")
        val appointmentListType = object : TypeToken<ArrayList<Appointment>>() {}.type


        if (forceRefresh) {
            service.getAppointment(appointment_id).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        Log.d("debug", "debug")
                    }
                    .doOnNext { results ->
                        run {
                            val editor = app.getSharedPreferences("HitPartnerApplication", Context.MODE_PRIVATE).edit()
                            editor.putString("appointments", Gson().toJson(results))
                            editor.commit()

                            val event = AppointmentEvent(AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, results)
                            bus.post(event)
                        }
                    }
                    .subscribe {
                        Log.d("debug", "debug")
                    }
        } else {
            appointments = Gson().fromJson(appointmentStr, appointmentListType)
            val event = AppointmentEvent(AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, AppointmentEvent.APPOINTMENT_LOAD_SUCCESS, appointments)
            bus.post(event)
        }
    }
}