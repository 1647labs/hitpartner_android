package net.hitpartner.app.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ApplicationModule {
    @Binds
    internal abstract fun bindContext(application: Application): Context


}