package net.hitpartner.app.di.register

import dagger.Binds
import dagger.Module

@Module
abstract class NewUserVerificationFragmentModule  {
    @Binds
    abstract fun newUserVerificationFragmentPresenter(presenter: RegisterFragmentPresenter) : RegisterFragmentContract.RegisterFragmentPresenter
}