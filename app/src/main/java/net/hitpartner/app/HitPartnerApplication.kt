package net.hitpartner.app

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.regions.Regions
import com.onesignal.OneSignal
import com.squareup.otto.Bus
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import net.hitpartner.app.aws.Constants
import net.hitpartner.app.di.DaggerApplicationComponent
import javax.inject.Inject

class HitPartnerApplication : DaggerApplication() {

    lateinit var userPool: CognitoUserPool

    @Inject
    lateinit var bus: Bus

    override fun onCreate() {
        super.onCreate()
        userPool = CognitoUserPool(this, Constants.COGNITO_POOL_ID, BuildConfig.clientId, BuildConfig.clientSecret, Regions.US_EAST_1)
        bus.register(this)

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val applicationComponent = DaggerApplicationComponent.builder().application(this).build()
        applicationComponent.inject(this)
        return applicationComponent
    }

}