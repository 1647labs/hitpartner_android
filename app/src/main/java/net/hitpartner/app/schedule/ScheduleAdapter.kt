package net.hitpartner.app.schedule

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import net.hitpartner.app.R
import net.hitpartner.app.model.Schedule

class ScheduleAdapter(private val scheduleKey: Set<String>,
                      private val scheduleMap: Map<String, List<Schedule>>,
                      private val context: Context) : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ScheduleAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.schedule_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return scheduleKey.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.icon_day.setImageResource(getIconDay(scheduleKey.toList()[position]))
        val scheduleList = scheduleMap[scheduleKey.toList()[position]]
        scheduleList?.forEach { schedule ->

            val locationTimeView = LocationTimeView(context)
            locationTimeView.findViewById<TextView>(R.id.venue_name).text = schedule.venue_name
            locationTimeView.findViewById<TextView>(R.id.day_name).text = schedule.day_name
            locationTimeView.findViewById<TextView>(R.id.start_time_text).text = schedule.start_time
            locationTimeView.findViewById<TextView>(R.id.end_time_text).text = schedule.end_time

            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 0, 0, 1)

            holder.time_location_container.addView(locationTimeView, params)

        }
    }

    private fun getIconDay(day_name: String): Int {
        var retIcon: Int = R.drawable.icons8_monday
        when (day_name.toLowerCase()) {
            "monday"    -> retIcon = R.drawable.icons8_monday
            "tuesday"   -> retIcon = R.drawable.icons8_tuesday
            "wednesday" -> retIcon = R.drawable.icons8_wednesday
            "thursday"  -> retIcon = R.drawable.icons8_thursday
            "friday"    -> retIcon = R.drawable.icons8_friday
            "saturday"  -> retIcon = R.drawable.icons8_saturday
            "sunday"    -> retIcon = R.drawable.icons8_sunday
        }
        return retIcon
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon_day = view.findViewById<ImageView>(R.id.icon_day)
        val time_location_container = view.findViewById<LinearLayout>(R.id.time_location_container)
    }
}