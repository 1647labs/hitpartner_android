package net.hitpartner.app.schedule

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import net.hitpartner.app.model.Venue


class ScheduleVenueAdapter(context: Context?, val resource: Int, val list: MutableList<Venue>) : ArrayAdapter<Venue>(context, resource, list) {

    private val layoutInflater: LayoutInflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        return getCustomView(position, convertView, parent)
    }

    private fun getCustomView(position: Int, customView: View?, parent: ViewGroup?): View {
        var itemView = customView
        if (itemView == null) {
            itemView = layoutInflater.inflate(resource, parent, false)
        }
        itemView?.findViewById<TextView>(android.R.id.text1)?.text = list.get(position).venue_name
        itemView?.findViewById<TextView>(android.R.id.text2)?.text = list.get(position).address
        return itemView!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, convertView, parent)
    }
}