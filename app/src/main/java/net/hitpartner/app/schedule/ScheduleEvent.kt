package net.hitpartner.app.schedule

import net.hitpartner.app.common.BaseEvent

class ScheduleEvent(tag: String, message: String? = null, data: Any? = null) : BaseEvent(tag, message, data) {
    companion object {
        const val SCHEDULE_CREATED: String = "Schedule Created"
        const val SCHEDULE_NEW: String = "Schedule New"
        const val SCHEDULE_LOAD_SUCCESS: String = "Schedule Load Success"
        const val SCHEDULE_LOAD_ERROR: String = "Schedule Load Error"
        const val SCHEDULE_UPDATE_ERROR: String = "Schedule Update Error"
    }
}