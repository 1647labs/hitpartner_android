package net.hitpartner.app.schedule

import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.edit_schedule_fragment_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.schedule.SchedulePresenter
import net.hitpartner.app.di.venue.VenuePresenter
import net.hitpartner.app.model.Schedule
import net.hitpartner.app.model.Venue
import net.hitpartner.app.venue.VenueEvent
import java.util.*
import javax.inject.Inject


class ScheduleEditFragment : BaseFragment() {

    @Inject
    lateinit var schedulePresenter: SchedulePresenter

    @Inject
    lateinit var venuePresenter: VenuePresenter

    private lateinit var schedule: Schedule

    override fun getLayoutResId() = R.layout.edit_schedule_fragment_view

    companion object {
        const val TAG: String = "ScheduleEditFragment"
        private lateinit var fragment: ScheduleEditFragment
        fun newInstance(): ScheduleEditFragment {
            fragment = ScheduleEditFragment()
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        schedule = Schedule()
        start_time_text.setOnClickListener { startTimeDialog(start_time_text) }
        end_time_text.setOnClickListener { endTimeDialog(end_time_text) }
        ok_button.setOnClickListener { view -> onButtonClickHandler(view) }
        cancel_button.setOnClickListener { view -> onButtonClickHandler(view) }

        button_monday.setOnClickListener { view -> onDayClickHandler(view) }
        button_tuesday.setOnClickListener { view -> onDayClickHandler(view) }
        button_wednesday.setOnClickListener { view -> onDayClickHandler(view) }
        button_thursday.setOnClickListener { view -> onDayClickHandler(view) }
        button_friday.setOnClickListener { view -> onDayClickHandler(view) }
        button_saturday.setOnClickListener { view -> onDayClickHandler(view) }
        button_sunday.setOnClickListener { view -> onDayClickHandler(view) }
    }

    private fun onDayClickHandler(view: View) {
        when (view) {
            button_monday -> {
                schedule.day_name = "Monday"
            }
            button_tuesday -> {
                schedule.day_name = "Tuesday"
            }
            button_wednesday -> {
                schedule.day_name = "Wednesday"
            }
            button_thursday -> {
                schedule.day_name = "Thursday"
            }
            button_friday -> {
                schedule.day_name = "Friday"
            }
            button_saturday -> {
                schedule.day_name = "Saturday"
            }
            button_sunday -> {
                schedule.day_name = "Sunday"
            }
        }
        selected_day_text.text = String.format("%s : %s", getString(R.string.select_day), schedule.day_name)
    }

    private fun onButtonClickHandler(view: View) {
        when (view) {
            ok_button -> {
                schedule.venue_name = (venue_spinner.selectedItem as Venue).venue_name
                schedule.venue_id = (venue_spinner.selectedItem as Venue).venue_id
                schedule.username = app.userPool.currentUser.userId
                schedule.member = false
                schedule.start_time = start_time_text.text.toString()
                schedule.end_time = end_time_text.text.toString()

                schedulePresenter.addSchedule(schedule)
            }
            cancel_button -> {
                activity!!.onBackPressed()
            }
        }
    }

    private fun startTimeDialog(incomingView: TextView) {
        // Get Current Time
        val c = Calendar.getInstance(Locale.getDefault())
        val mHour = c.get(Calendar.HOUR)
        val mMinute = c.get(Calendar.MINUTE)

        val am_pm = when (c.get(Calendar.AM_PM)) {
            Calendar.AM -> "AM"
            else -> "PM"
        }

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    run {
                        val timeText = String.format("%02d:%02d: %s", hourOfDay, minute, am_pm)
                        incomingView.text = timeText
                    }
                }, mHour, mMinute, false)
        timePickerDialog.show()
    }

    private fun endTimeDialog(incomingView: TextView) {
        // Get Current Time
        val c = Calendar.getInstance(Locale.getDefault())
        val mHour = c.get(Calendar.HOUR)
        val mMinute = c.get(Calendar.MINUTE)

        val am_pm = when (c.get(Calendar.AM_PM)) {
            Calendar.AM -> "AM"
            else -> "PM"
        }

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    run {
                        val timeText = String.format("%02d:%02d: %s", hourOfDay, minute, am_pm)
                        incomingView.text = timeText
                    }
                }, mHour, mMinute, false)
        timePickerDialog.show()
    }

    override fun onResume() {
        super.onResume()
        venuePresenter.getVenues(true)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Venue>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                val adapter = ScheduleVenueAdapter(context, android.R.layout.simple_list_item_2, list.toMutableList())
                venue_spinner.adapter = adapter
            }
        }
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
        }
    }
}