package net.hitpartner.app.schedule

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_schedule_list_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.schedule.SchedulePresenter
import net.hitpartner.app.model.Schedule
import javax.inject.Inject

class ScheduleListFragment : BaseFragment() {

    @Inject
    lateinit var presenter: SchedulePresenter

    override fun getLayoutResId() = R.layout.fragment_schedule_list_view

    companion object {
        private lateinit var fragment: ScheduleListFragment
        const val TAG: String = "ScheduleListFragment"
        fun newInstance(): ScheduleListFragment {
            fragment = ScheduleListFragment()
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fab.setOnClickListener { view -> onClickListener(view) }
    }

    private fun onClickListener(view: View) {
        when (view) {
            fab -> {
                bus.post(ScheduleEvent(ScheduleEvent.SCHEDULE_NEW, ScheduleEvent.SCHEDULE_NEW, null))
                Snackbar.make(fab, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getSchedule(true)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is ScheduleEvent && e.tag == ScheduleEvent.SCHEDULE_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Schedule>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                schedule_recycler_view.layoutManager = llm

                val scheduleMap  = list.groupBy { it.day_name }
                val scheduleKeys = scheduleMap.keys

                schedule_recycler_view.adapter = ScheduleAdapter(scheduleKeys,scheduleMap, this.context!!)
            }
        }
    }
}