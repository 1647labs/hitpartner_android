package net.hitpartner.app.schedule

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import net.hitpartner.app.R

class LocationTimeView @JvmOverloads constructor(context: Context,
                                                 attrs: AttributeSet? = null,
                                                 defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context)
                .inflate(R.layout.item_location_time, this, true)
        orientation = VERTICAL


    }
}