package net.hitpartner.app.venue

import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.venue.VenuePresenter
import net.hitpartner.app.model.Venue
import javax.inject.Inject

class VenueFragment : BaseFragment() {

    private lateinit var venue: Venue

    @Inject
    lateinit var presenter: VenuePresenter

    companion object {
        const val TAG: String = "VenueFragment"
        private lateinit var fragment: VenueFragment
        fun newInstance(): VenueFragment {
            fragment = VenueFragment()
            return fragment
        }
    }

    override fun getLayoutResId() = R.layout.venue_view

    override fun eventHandler(e: BaseEvent) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}