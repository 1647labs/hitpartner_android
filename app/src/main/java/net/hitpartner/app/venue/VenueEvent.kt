package net.hitpartner.app.venue

import net.hitpartner.app.common.BaseEvent

class VenueEvent(tag: String, message: String? = null, data: Any? = null) : BaseEvent(tag, message, data) {
    companion object {
        const val VENUE_CREATED: String = "Venue Created"
        const val VENUE_NEW: String = "Venue New"
        const val VENUE_LOAD_SUCCESS: String = "Venue Load Success"
        const val VENUE_LOAD_ERROR: String = "Venue Load Error"
    }
}