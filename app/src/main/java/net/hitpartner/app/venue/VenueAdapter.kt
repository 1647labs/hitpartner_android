package net.hitpartner.app.venue

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.hitpartner.app.R
import net.hitpartner.app.model.Venue

class VenueAdapter constructor(private val items: ArrayList<Venue>) : RecyclerView.Adapter<VenueAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.venue_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.venue_name.text = items.get(position).venue_name
        holder.address.text = items.get(position).address
        holder.city.text = items.get(position).city
        holder.phone.text = items.get(position).phone
        holder.state.text = items.get(position).state
        holder.zipcode.text = items.get(position).zipcode
        holder.contact.text = items.get(position).contact
//        holder.picture_url.text = items.get(position).picture_url
//        holder.website.text = items.get(position).website
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val venue_name = view.findViewById<TextView>(R.id.venue_name)
        val address = view.findViewById<TextView>(R.id.address)
        val city = view.findViewById<TextView>(R.id.city)
        val phone = view.findViewById<TextView>(R.id.phone)
        val state = view.findViewById<TextView>(R.id.state)
        val zipcode = view.findViewById<TextView>(R.id.zipcode)
        val contact = view.findViewById<TextView>(R.id.contact)
//        val picture_url = view.findViewById<TextView>(R.id.picture_url)
//        val website = view.findViewById<TextView>(R.id.website)
    }

}