package net.hitpartner.app.venue

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_venues_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.venue.VenuePresenter
import net.hitpartner.app.model.Venue
import javax.inject.Inject

class VenueListFragment : BaseFragment() {

    @Inject
    lateinit var presenter: VenuePresenter

    override fun getLayoutResId(): Int = R.layout.fragment_venues_view

    companion object {
        private lateinit var fragment: VenueListFragment
        const val TAG: String = "VenueListFragment"
        fun newInstance(): VenueListFragment {
            fragment = VenueListFragment()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        view?.findViewById<FloatingActionButton>(R.id.fab)?.setOnClickListener { v ->
            Snackbar.make(v, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.getVenues(true)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is VenueEvent && e.tag == VenueEvent.VENUE_LOAD_SUCCESS) {
            activity?.runOnUiThread {
                val list = e.data as ArrayList<Venue>
                val llm = LinearLayoutManager(activity)
                llm.orientation = LinearLayoutManager.VERTICAL
                venue_recycler_view.layoutManager = llm
                venue_recycler_view.adapter = VenueAdapter(list)
            }
        }
    }
}