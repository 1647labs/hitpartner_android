package net.hitpartner.app.profile

import net.hitpartner.app.common.BaseEvent

class ProfileEvent(tag: String, message: String? = null, data: Any? = null) : BaseEvent(tag, message, data) {
    companion object {
        const val PROFILE_UPDATED: String = "Profile Updated"
        const val PROFILE_CREATED: String = "Profile Created"
        const val PROFILE_LOAD_SUCCESS: String = "Profile Load Success"
        const val PROFILES_LOAD_SUCCESS: String = "Profiles Load Success"
    }
}