package net.hitpartner.app.profile

import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_profile_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.profile.ProfilePresenter
import net.hitpartner.app.model.Profile
import javax.inject.Inject

class ProfileFragment : BaseFragment() {
    private lateinit var userProfile: Profile

    @Inject
    lateinit var presenter: ProfilePresenter

    companion object {
        const val TAG: String = "ProfileFragment"
        private lateinit var fragment: ProfileFragment
        fun newInstance(): ProfileFragment {
            fragment = ProfileFragment()
            return fragment
        }
    }

    override fun getLayoutResId(): Int = R.layout.fragment_profile_view

    override fun onResume() {
        super.onResume()
        val user = app.userPool.currentUser
        presenter.getUserProfile(user.userId)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is ProfileEvent && e.data is Profile)
            userProfile = e.data

        first_name_text.text = userProfile.firstname
        last_name_text.text = userProfile.lastname
        email_text.text = userProfile.email
        address_text.text = userProfile.address
        phone_number_text.text = userProfile.phone_number
        city_text.text = userProfile.city
        state_text.text = userProfile.state
        zipcode_text.text = userProfile.zipcode
        hp_level_text.text = userProfile.hp_level

    }
}