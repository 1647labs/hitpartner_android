package net.hitpartner.app.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.gson.Gson
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.edit_profile_view.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.profile.ProfilePresenter
import net.hitpartner.app.model.Profile
import javax.inject.Inject

class EditProfileFragment : BaseFragment() {
    private lateinit var userProfile: Profile
    private var isUpdate: Boolean = false

    @Inject
    lateinit var presenter: ProfilePresenter

    companion object {
        private lateinit var fragment: EditProfileFragment
        const val TAG: String = "EditProfileFragment"
        fun newInstance(): EditProfileFragment {
            fragment = EditProfileFragment()
            return fragment
        }
    }

    override fun getLayoutResId(): Int = R.layout.edit_profile_view

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        view?.findViewById<Button>(R.id.save_button)?.setOnClickListener { saveButtonClickHandler() }
        view?.findViewById<Button>(R.id.cancel_button)?.setOnClickListener { cancelButtonClickHandler() }

        return view
    }


    override fun onResume() {
        super.onResume()
        val profileString = arguments?.getString("profile")
        if (profileString != null) {
            isUpdate = true
            userProfile = Gson().fromJson<Profile>(profileString, Profile::class.java)
            updateView(userProfile)
        }
    }

    private fun updateView(userProfile: Profile) {
        first_name_text.setText(userProfile.firstname)
        last_name_text.setText(userProfile.lastname)
        email_text.setText(userProfile.email)
        address_text.setText(userProfile.address)
        phone_number_text.setText(userProfile.phone_number)
        city_text.setText(userProfile.city)
        state_text.setText(userProfile.state)
        zipcode_text.setText(userProfile.zipcode)
        hp_level_text.setText(userProfile.hp_level)
    }

    private fun cancelButtonClickHandler() {
        fragmentManager?.popBackStack()
    }

    private fun saveButtonClickHandler() {

        val address = address_text.text.toString()
        val city = city_text.text.toString()
        val state = state_text.text.toString()
        val zipcode = zipcode_text.text.toString()
        val email = email_text.text.toString()
        val firstName = first_name_text.text.toString()
        val lastName = last_name_text.text.toString()
        val hp_level = hp_level_text.text.toString()

        var phoneNumber = phone_number_text.text.toString()

        if (!phoneNumber.isNullOrBlank()) {
            val pnu = PhoneNumberUtil.getInstance()
            val pn = pnu.parse(phoneNumber, "US")
            phoneNumber = pnu.format(pn, PhoneNumberUtil.PhoneNumberFormat.E164)
        }

        val profile = Profile(address, city, email, firstName, lastName, hp_level, state, zipcode, phoneNumber, userProfile.username, userProfile.profile_id)

        if (isUpdate)
            presenter.updateUserProfile(profile)
        else
            presenter.saveUserProfile(profile)
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        if (e is ProfileEvent) {
            if (ProfileEvent.PROFILE_UPDATED == e.tag)
                activity?.supportFragmentManager?.popBackStack()
        }
    }
}