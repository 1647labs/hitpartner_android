package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.model.Profile
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MockProfileService {

    @GET("/profile")
    fun getUserProfile(): Observable<List<Profile>>

    companion object Factory {

        fun create(): MockProfileService {
            val okHttpBuilder = OkHttpClient.Builder()
            val okHttpClient = okHttpBuilder.build()

            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.PROFILE_MOCK_SERVICE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(MockProfileService::class.java)
        }
    }
}