package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.model.Venue
import net.hitpartner.app.model.VenueResponse
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MockVenueService {

    @GET("/venue")
    fun getVenues(): Observable<List<Venue>>

    companion object Factory {

        fun create(): MockVenueService {
            val okHttpBuilder = OkHttpClient.Builder()
            val okHttpClient = okHttpBuilder.build()

            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.VENUE_MOCK_SERVICE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(MockVenueService::class.java)
        }
    }
}