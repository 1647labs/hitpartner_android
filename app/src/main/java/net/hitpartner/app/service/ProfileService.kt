package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.model.Profile
import net.hitpartner.app.model.ProfileResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ProfileService {

    @GET("/dev/profile/")
    fun getUserProfile(): Observable<ProfileResponse>

    @GET("/dev/profile/{profile_id}")
    fun getUserProfileByEmail(@Path("profile_id") profile_id: String): Observable<Profile>

    @POST("/dev/profile")
    fun addProfileDetails(@Body profile: Profile): Observable<Profile>

    @PUT("/dev/profile")
    fun updateProfileDetails(@Body profile: Profile): Observable<Profile>

    @GET("/dev/profile/hitpartner")
    fun getHitPartners(): Observable<ProfileResponse>

    companion object Factory {
        fun create(): ProfileService {
            val okHttpBuilder = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(CognitoInterceptor())
            val okHttpClient = okHttpBuilder.build()

            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.HITPARTNER_PROFILE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(ProfileService::class.java)
        }

        private class CognitoInterceptor : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val response: Response

                val newRequest = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("x-cognito-authorization", AWSCognitoUtility.userSession.idToken.jwtToken)
                        .build()

                response = chain.proceed(newRequest)

                return response
            }
        }
    }
}