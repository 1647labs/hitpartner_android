package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.model.Appointment
import net.hitpartner.app.model.AppointmentResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface AppointmentService {

    @GET("/dev/appointment/{appointment_id}")
    fun getAppointment(@Path("appointment_id") appointment_id: String): Observable<AppointmentResponse>

    @GET("/dev/appointment")
    fun getAppointment(): Observable<AppointmentResponse>


    @POST("/dev/appointment/")
    fun addAppointment(@Body appointment: Appointment): Observable<Appointment>

    companion object Factory {
        fun create(): AppointmentService {
            val okHttpBuilder = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(CognitoInterceptor())
            val okHttpClient = okHttpBuilder.build()

            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.HITPARTNER_APPOINTMENT_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(AppointmentService::class.java)
        }

        private class CognitoInterceptor : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val response: Response

                val newRequest = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("x-cognito-authorization", AWSCognitoUtility.userSession.idToken.jwtToken)
                        .build()

                response = chain.proceed(newRequest)

                return response
            }
        }
    }
}