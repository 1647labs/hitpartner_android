package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.model.Schedule
import net.hitpartner.app.model.ScheduleResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ScheduleService {

    @GET("/dev/schedule")
    fun getSchedule(): Observable<ScheduleResponse>

    @GET("/dev/schedule/{schedule_id}")
    fun getScheduleById(@Path("schedule_id") schedule_id: String): Observable<ScheduleResponse>

    @POST("/dev/schedule/")
    fun addSchedule(@Body schedule: Schedule): Observable<Schedule>

    @PUT("/dev/schedule/")
    fun updateSchedule(@Body schedule: Schedule): Observable<Schedule>

    companion object Factory {
        fun create(): ScheduleService {
            val okHttpBuilder = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(CognitoInterceptor())
            val okHttpClient = okHttpBuilder.build()
            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(BuildConfig.HITPARTNER_SCHEDULE_URL)
                    .build()
            return retrofit.create(ScheduleService::class.java)
        }

        private class CognitoInterceptor : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val response: Response

                val newRequest = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("x-cognito-authorization", AWSCognitoUtility.userSession.idToken.jwtToken)
                        .build()

                response = chain.proceed(newRequest)

                return response
            }
        }
    }
}