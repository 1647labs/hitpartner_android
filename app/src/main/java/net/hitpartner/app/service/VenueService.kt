package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.model.Schedule
import net.hitpartner.app.model.Venue
import net.hitpartner.app.model.VenueResponse
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface VenueService {

    @GET("/dev/venue")
    fun getVenues(): Observable<VenueResponse>

    @GET("/dev/venue/{venue_id}")
    fun getVenueByName(@Path("venue_id") venue_id: String): Observable<VenueResponse>

    companion object Factory {

        fun create(): VenueService {
            val okHttpBuilder = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(CognitoInterceptor())
            val okHttpClient = okHttpBuilder.build()
            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(BuildConfig.HITPARTNER_VENUE_URL)
                    .build()
            return retrofit.create(VenueService::class.java)
        }

        private class CognitoInterceptor : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val response: Response

                val newRequest = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("x-cognito-authorization", AWSCognitoUtility.userSession.idToken.jwtToken)
                        .build()

                response = chain.proceed(newRequest)

                return response
            }
        }
    }
}