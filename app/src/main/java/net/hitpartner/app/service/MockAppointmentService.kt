package net.hitpartner.app.service

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import net.hitpartner.app.BuildConfig
import net.hitpartner.app.model.Appointment
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface MockAppointmentService {

    @GET("/appointment/{appointment_id}")
    fun getAppointment(@Path("appointment_id") appointment_id: String): Observable<List<Appointment>>

    @GET("/appointment")
    fun getAppointment(): Observable<List<Appointment>>

    companion object Factory {
        fun create(): MockAppointmentService {
            val okHttpBuilder = OkHttpClient.Builder()
            val okHttpClient = okHttpBuilder.build()

            val retrofit = retrofit2.Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.APPOINTMENT_MOCK_SERVICE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(MockAppointmentService::class.java)
        }
    }
}