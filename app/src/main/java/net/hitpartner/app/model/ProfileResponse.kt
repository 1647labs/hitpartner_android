package net.hitpartner.app.model

import com.google.gson.annotations.SerializedName

class ProfileResponse {
    @SerializedName("profile")
    lateinit var profileList:List<Profile>
}