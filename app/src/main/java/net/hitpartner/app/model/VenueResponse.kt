package net.hitpartner.app.model

import com.google.gson.annotations.SerializedName

class VenueResponse {
    @SerializedName("venue")
    lateinit var venueList: List<Venue>
}