package net.hitpartner.app.model

data class Venue(
        val venue_id: String = "",
        val address: String = "",
        val city: String = "",
        val contact: String = "",
        val lat: String = "",
        val lng: String = "",
        val phone: String = "",
        val picture_url: String = "",
        val state: String = "",
        val venue_name: String = "",
        val website: String = "",
        val zipcode: String = "")