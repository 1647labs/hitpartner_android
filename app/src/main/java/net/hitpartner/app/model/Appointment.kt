package net.hitpartner.app.model

import java.io.Serializable

data class Appointment(
        var username: String = "",
        var appointment_id: String = "",
        var hitpartner_id: String = "",
        var hitpartner_name: String = "",
        var hitseeker_id: String = "",
        var venue_id: String = "",
        var venue_name: String = "",
        var venue_location: String = "",
        var venue_address: String = "2150 S Canalport",
        var start_time: String = "",
        var end_time: String = "",
        var duration: String = "",
        var rate: String = "",
        var rate_type: String = "",
        var datecreated: String = "",
        var datemodified: String = "",
        var datecreated_timestamp: String = "",
        var status: String = "") : Serializable