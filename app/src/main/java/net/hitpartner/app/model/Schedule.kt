package net.hitpartner.app.model

data class Schedule(
        var schedule_id: String = "0000-0000-0000-0000",
        var username: String = "0000-0000-0000-0000",
        var venue_id: String = "0000-0000-0000-0000",
        var day_name: String = "Monday",
        var member: Boolean = true,
        var start_time: String = "11:00am",
        var end_time: String = "1:00pm",
        var venue_name: String = "Blue1647")