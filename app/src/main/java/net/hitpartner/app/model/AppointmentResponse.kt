package net.hitpartner.app.model

import com.google.gson.annotations.SerializedName

class AppointmentResponse {
    @SerializedName("appointment")
    lateinit var appointmentList:List<Appointment>
}