package net.hitpartner.app.model

import com.google.gson.annotations.SerializedName

class ScheduleResponse {
    @SerializedName("schedule")
    lateinit var scheduleList:List<Schedule>
}