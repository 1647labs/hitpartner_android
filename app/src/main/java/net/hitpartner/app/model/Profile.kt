package net.hitpartner.app.model

import java.io.Serializable

data class Profile(
        var address: String = "",
        var city: String = "",
        var email: String = "",
        var firstname: String = "",
        var lastname: String = "",
        var hp_level: String = "",
        var state: String = "",
        var zipcode: String = "",
        var phone_number: String = "",
        var username: String = "",
        var profile_id: String = "",
        var latitude: String = "41.881832",
        var longitude: String = "-87.623177",
        var is_hitpartner: Boolean = false,
        var is_hitseeker: Boolean = false
) : Serializable
