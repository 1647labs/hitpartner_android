package net.hitpartner.app.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.amazonaws.services.cognitoidentityprovider.model.UserNotConfirmedException
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_login_view.*
import net.hitpartner.app.NavigationActivity
import net.hitpartner.app.R
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.aws.CognitoEvent
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.login.LoginFragmentPresenter
import javax.inject.Inject

/**
 * Created by markusmcgee on 3/31/18.
 */
class LoginFragment : BaseFragment() {

    @Inject
    lateinit var presenter: LoginFragmentPresenter

    private lateinit var registerFragment: RegisterFragment

    companion object {
        private lateinit var fragment: LoginFragment
        fun newInstance(): LoginFragment {
            fragment = LoginFragment()
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        view?.findViewById<AppCompatButton>(R.id.login_button)?.setOnClickListener { onLoginButtonClickHandler() }
        view?.findViewById<TextView>(R.id.register_button)?.setOnClickListener { onRegisterButtonClickHandler() }
        return view
    }

    override fun getLayoutResId(): Int = R.layout.fragment_login_view

    private fun onLoginButtonClickHandler() {
        (activity as LoginActivity).showProgressBar()
        AWSCognitoUtility(app.userPool, bus).signInUserInBackground(username_text?.text.toString(), password_text.text.toString())
    }

    private fun onRegisterButtonClickHandler() {
        registerFragment = RegisterFragment.newInstance()
        activity?.supportFragmentManager?.beginTransaction()?.add(R.id.fragment_container, registerFragment)?.commit()
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        (activity as LoginActivity).closeProgressBar()
        if (e.tag.equals(CognitoEvent.ON_SIGN_IN_SUCCESS)) {

            (activity as LoginActivity).runOnUiThread {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NO_HISTORY
                startActivity(intent)
            }
/*
            (activity as LoginActivity).runOnUiThread {
                val intent = Intent(activity, ProfileActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NO_HISTORY
                startActivity(intent)
            }
*/

        }
        else if (e.tag.equals(CognitoEvent.ON_SIGNI_IN_FAILURE) ||
                 e.tag.equals(CognitoEvent.PASSWORD_BLANK) ||
                 e.tag.equals(CognitoEvent.USERNAME_BLANK)) {
            if (e.data is UserNotConfirmedException) {
                Toast.makeText(activity, "Check your email for verification code.", Toast.LENGTH_SHORT).show()

                (activity as LoginActivity).runOnUiThread {
                    val fragment = NewUserVerificationFragment.newInstance()
                    val bundle = Bundle()
                    bundle.putString("email", username_text.text.toString())
                    fragment.arguments = bundle
                    (activity as LoginActivity).supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()
                }
            }
        }
    }
}
