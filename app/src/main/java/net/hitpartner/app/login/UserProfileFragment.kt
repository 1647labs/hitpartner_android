package net.hitpartner.app.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_user_profile.*
import net.hitpartner.app.R
import net.hitpartner.app.aws.CognitoEvent
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.register.RegisterFragmentPresenter
import net.hitpartner.app.model.Profile
import javax.inject.Inject

class UserProfileFragment : BaseFragment() {

    @Inject
    lateinit var presenter: RegisterFragmentPresenter

    override fun getLayoutResId() = R.layout.fragment_user_profile

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        view?.findViewById<Button>(R.id.button_register)?.setOnClickListener { addUpdateUserProfileButtonClickHandler() }
        return view
    }
    private fun addUpdateUserProfileButtonClickHandler() {

        val address = address_text.text.toString()
        val city = city_text.text.toString()
        val state = state_text.text.toString()
        val zipcode = zipcode_text.text.toString()
        val email = email_text.text.toString()
        val firstName = first_name_text.text.toString()
        val lastName = last_name_text.text.toString()
        val hp_level = hitpartner_level_text.text.toString()

        var phoneNumber = phone_number_text.text.toString()
        val pnu = PhoneNumberUtil.getInstance()
        val pn = pnu.parse(phoneNumber, "US")
        phoneNumber = pnu.format(pn, PhoneNumberUtil.PhoneNumberFormat.E164)

        presenter.addProfileDetails(Profile(address,city,email,firstName,lastName,hp_level,state,zipcode,phoneNumber))

    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        Log.d("eventhandle", e.message)
        when (e.message?.isNotBlank()) {
            true -> Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
        }

        activity?.intent?.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        if (e.tag.equals(CognitoEvent.ON_SIGN_IN_SUCCESS)) {
            Toast.makeText(activity, "Login Successful", Toast.LENGTH_LONG).show()
        } else if (e.tag.equals(CognitoEvent.ON_SIGNI_IN_FAILURE)) {
            Toast.makeText(activity, "Login failure", Toast.LENGTH_LONG).show()
        }
    }
}