package net.hitpartner.app.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_confirm_registration.*
import net.hitpartner.app.R
import net.hitpartner.app.aws.AWSCognitoUtility
import net.hitpartner.app.aws.CognitoEvent
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment

class ConfirmUserRegistrationFragment : BaseFragment() {
    override fun getLayoutResId(): Int = R.layout.fragment_confirm_registration

    lateinit var userNameText: EditText
    lateinit var confirmationText: EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        view?.findViewById<Button>(R.id.confirmation_code_button)?.setOnClickListener { onConfirmationButtonClick() }
        return view
    }

    fun onConfirmationButtonClick() {
        AWSCognitoUtility(app.userPool, bus).confirmSignUpInBackground(register_email_text.text.toString(), confirmationText.text.toString())
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        Toast.makeText(app.applicationContext, e.message, Toast.LENGTH_LONG).show()
        if (e.tag == CognitoEvent.ON_CONFIRM_SUCCESS) {
            val intent = Intent(app.applicationContext, LoginActivity::class.java)
            intent.putExtra("username", userNameText.text.toString())
            startActivity(intent)
        }
    }
}