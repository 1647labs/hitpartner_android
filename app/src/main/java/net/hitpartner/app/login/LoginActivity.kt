package net.hitpartner.app.login

import android.os.Bundle
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseActivity
import net.hitpartner.app.common.BaseEvent

class LoginActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.hitpartner_login_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fragment = LoginFragment.newInstance()
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()
    }

    override fun eventHandler(e: BaseEvent) {
        //To change body of created functions use File | Settings | File Templates.
        //TODO("not implemented")
    }


}