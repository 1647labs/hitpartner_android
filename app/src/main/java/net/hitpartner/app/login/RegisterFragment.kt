package net.hitpartner.app.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_register_user.*
import net.hitpartner.app.R
import net.hitpartner.app.aws.CognitoEvent
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.register.RegisterFragmentPresenter
import javax.inject.Inject


/**
 * Created by Rakesh Das on 3/31/18.
 */
class RegisterFragment : BaseFragment() {

    @Inject
    lateinit var presenter: RegisterFragmentPresenter

    private lateinit var userType: String

    companion object {
        private lateinit var fragment: RegisterFragment
        fun newInstance(): RegisterFragment {
            fragment = RegisterFragment()
            return fragment
        }
    }

    override fun getLayoutResId(): Int = R.layout.fragment_register_user

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        view?.findViewById<Button>(R.id.button_register)?.setOnClickListener { registerButtonClickHandler() }
        return view
    }

    private fun registerButtonClickHandler() {
        //get data from text fields
        if (confirm_password_text.text.toString() != password_text.text.toString()) {
            Toast.makeText(app.applicationContext, "Password and confirm passwords do not match.  Please try again.", Toast.LENGTH_LONG).show()
            return
        }

        val email = email_text.text.toString()
        val firstName = first_name_text.text.toString()
        val lastName = last_name_text.text.toString()
        val password = password_text.text.toString()
        var phoneNumber = phone_number_text.text.toString()

        val pnu = PhoneNumberUtil.getInstance()
        val pn = pnu.parse(phoneNumber, "US")
        phoneNumber = pnu.format(pn, PhoneNumberUtil.PhoneNumberFormat.E164)

        userType = if (radio_hitseeker.isSelected)
            "hit-seeker"
        else
            "hit-partner"

        presenter.registerUser(email, password, phoneNumber)

    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        Log.d("eventhandle", e.message)
        when (e.message?.isNotBlank()) {
            true -> Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
        }

        activity?.intent?.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        if (e.tag.equals(CognitoEvent.ON_SIGN_IN_SUCCESS)) {
            Toast.makeText(activity, "Login Successful", Toast.LENGTH_LONG).show()
        } else if (e.tag.equals(CognitoEvent.ON_SIGNI_IN_FAILURE)) {
            Toast.makeText(activity, "Login failure", Toast.LENGTH_LONG).show()
        }
    }

    @Subscribe
    fun userRegistrationEventHandler(e: String) {
        Log.d("eventhandle", e)
        if (e == "User is not confirmed") {
            Toast.makeText(activity, "Confirmation email sent!", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(activity, "Something went wrong...", Toast.LENGTH_LONG).show()
        }
    }

}