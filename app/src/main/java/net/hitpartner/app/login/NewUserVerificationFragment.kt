package net.hitpartner.app.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.amazonaws.services.cognitoidentityprovider.model.ExpiredCodeException
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_new_user_verification.*
import net.hitpartner.app.R
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.common.BaseFragment
import net.hitpartner.app.di.register.RegisterFragmentPresenter
import javax.inject.Inject

class NewUserVerificationFragment : BaseFragment() {

    @Inject
    lateinit var presenter: RegisterFragmentPresenter

    companion object {
        private lateinit var fragment: NewUserVerificationFragment
        fun newInstance(): NewUserVerificationFragment {
            fragment = NewUserVerificationFragment()
            return fragment
        }
    }

    override fun getLayoutResId(): Int = R.layout.fragment_new_user_verification

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        view?.findViewById<Button>(R.id.verify_button)?.setOnClickListener { verifyButtonClickHandler() }
        view?.findViewById<Button>(R.id.resend_code_button)?.setOnClickListener { resendCodeButtonClickHandler() }
        view?.findViewById<TextView>(R.id.email_text)?.text = arguments?.get("email").toString()

        return view
    }

    private fun resendCodeButtonClickHandler() {
        presenter.resendCode(email_text.text.toString())
    }

    private fun verifyButtonClickHandler() {
        presenter.verifyUser(email_text.text.toString(), verify_code_text.text.toString())
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        Toast.makeText(activity, "Check your email for verification code.", Toast.LENGTH_SHORT).show()

        if(e.data is ExpiredCodeException){
            Toast.makeText(activity,"Code has been used already.  Resend code.", Toast.LENGTH_LONG).show()
        }

//        (activity as LoginActivity).runOnUiThread {
//            val fragment = NewUserVerificationFragment.newInstance()
//            val bundle = Bundle()
//            bundle.putString("email", username_text.text.toString())
//            fragment.arguments = bundle
//            (activity as LoginActivity).supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()
//        }

    }
}

