package net.hitpartner.app

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_appointments_view.*
import kotlinx.android.synthetic.main.titlebar.*
import net.hitpartner.app.appointment.*
import net.hitpartner.app.common.BaseActivity
import net.hitpartner.app.common.BaseEvent
import net.hitpartner.app.model.Profile
import net.hitpartner.app.profile.EditProfileFragment
import net.hitpartner.app.profile.ProfileEvent
import net.hitpartner.app.profile.ProfileFragment
import net.hitpartner.app.schedule.ScheduleEditFragment
import net.hitpartner.app.schedule.ScheduleEvent
import net.hitpartner.app.schedule.ScheduleListFragment
import net.hitpartner.app.venue.VenueEditFragment
import net.hitpartner.app.venue.VenueEvent
import net.hitpartner.app.venue.VenueFragment
import net.hitpartner.app.venue.VenueListFragment
import java.io.Serializable

class NavigationActivity : BaseActivity() {

    private lateinit var userProfile: Profile

    private lateinit var appointmentListFragment: AppointmentListFragment
    private lateinit var appointmentCalendarFragment: AppointmentCalendarFragment

    private lateinit var editProfileFragment: EditProfileFragment
    private lateinit var profileFragment: ProfileFragment

    private lateinit var venueListFragment: VenueListFragment
    private lateinit var venueFragment: VenueFragment
    private lateinit var venueEditFragment: VenueEditFragment

    private lateinit var scheduleListFragment: ScheduleListFragment

    override fun getLayoutResId(): Int = R.layout.hitpartner_activity_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appointment_calendar_button.visibility = View.VISIBLE
        appointment_calendar_button.setOnClickListener { view -> toolbarButtonClickHandler(view) }
        appointment_list_button.setOnClickListener { view -> toolbarButtonClickHandler(view) }
        edit_profile_button.setOnClickListener { view -> toolbarButtonClickHandler(view) }
        edit_schedule_button.setOnClickListener { view -> toolbarButtonClickHandler(view) }
        edit_appointment_button.setOnClickListener { view -> toolbarButtonClickHandler(view) }


        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigation_view)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId = R.id.navigation_appointments
    }

    private fun toolbarButtonClickHandler(view: View) {
        when (view) {
            appointment_list_button -> {
                appointmentListFragment = AppointmentListFragment.newInstance()
                openFragment(appointmentListFragment)
                toolbar_title.text = getString(R.string.appointments)
                appointment_list_button.visibility = View.GONE
                appointment_calendar_button.visibility = View.VISIBLE
                edit_appointment_button.visibility = View.GONE
            }
            appointment_calendar_button -> {
                appointmentCalendarFragment = AppointmentCalendarFragment.newInstance()
                openFragment(appointmentCalendarFragment)
                toolbar_title.text = getString(R.string.appointments_calendar)
                appointment_calendar_button.visibility = View.GONE
                appointment_list_button.visibility = View.VISIBLE
                edit_appointment_button.visibility = View.GONE
            }
            edit_profile_button -> {
                editProfileFragment = EditProfileFragment.newInstance()
                val bundle = Bundle()
                bundle.putString("profile", Gson().toJson(userProfile))
                toolbar_title.text = getString(R.string.profile)
                editProfileFragment.arguments = bundle
                openFragment(editProfileFragment)
            }
            edit_schedule_button -> {
                scheduleListFragment = ScheduleListFragment.newInstance()
                openFragment(scheduleListFragment)
                toolbar_title.text = getString(R.string.my_availability)
                edit_profile_button.visibility = View.GONE
                edit_schedule_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE
            }
            edit_appointment_button -> {
                Snackbar.make(fab, "Edit Appointment Called", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
            }
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_appointments -> {
                appointmentListFragment = AppointmentListFragment.newInstance()
                appointment_calendar_button.visibility = View.VISIBLE

                edit_profile_button.visibility = View.GONE
                edit_schedule_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE

                toolbar_title.text = getString(R.string.appointments)
                openFragment(appointmentListFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                profileFragment = ProfileFragment.newInstance()
                toolbar_title.text = getString(R.string.profile)

                appointment_calendar_button.visibility = View.GONE
                appointment_list_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE

                edit_profile_button.visibility = View.VISIBLE
                edit_schedule_button.visibility = View.VISIBLE

                openFragment(profileFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_venue -> {
                venueListFragment = VenueListFragment.newInstance()
                toolbar_title.text = getString(R.string.venue)

                appointment_calendar_button.visibility = View.GONE
                appointment_list_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE
                edit_profile_button.visibility = View.GONE
                edit_schedule_button.visibility = View.GONE

                openFragment(venueListFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    @Subscribe
    override fun eventHandler(e: BaseEvent) {
        when (e) {
            is ProfileEvent -> {
                if (e.data is Profile)
                    userProfile = e.data
            }
            is AppointmentEvent -> {
                if (e.tag == AppointmentEvent.APPOINTMENT_NEW) {
                    val fragment = AppointmentEditFragment.newInstance()
                    val bundle = Bundle()
                    appointment_calendar_button.visibility = View.GONE
                    edit_appointment_button.visibility = View.GONE
                    appointment_list_button.visibility = View.GONE
                    bundle.putString("appointmentType", e.data as String)
                    fragment.arguments = bundle
                    toolbar_title.text = getString(R.string.new_appointment)
                    openFragment(fragment)
                } else if (e.tag == AppointmentEvent.APPOINTMENT_DETAIL) {
                    val fragment = AppointmentFragment.newInstance()
                    val bundle = Bundle()

                    appointment_calendar_button.visibility = View.GONE
                    appointment_list_button.visibility = View.GONE
                    edit_appointment_button.visibility = View.GONE

                    edit_appointment_button.visibility = View.VISIBLE

                    bundle.putSerializable("appointment", e.data as Serializable?)
                    fragment.arguments = bundle
                    openFragment(fragment)
                }
            }
            is VenueEvent -> {
                appointment_calendar_button.visibility = View.GONE
                appointment_list_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE

                if (e.tag == VenueEvent.VENUE_NEW) {
                    openFragment(VenueEditFragment.newInstance())
                }
            }
            is ScheduleEvent -> {
                appointment_calendar_button.visibility = View.GONE
                appointment_list_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE
                edit_appointment_button.visibility = View.GONE
                if (e.tag == ScheduleEvent.SCHEDULE_NEW) {
                    openFragment(ScheduleEditFragment.newInstance())
                }
            }
        }
    }
}